import React, { Component } from 'react';
import { siteUrl } from './Settings';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import  SimpleCard from './SimpleCard';
import Grid from '@material-ui/core/Grid';

class Home extends Component {
  render() {
    const bannerImages = [];
    //const data = this.props.fetchInitialData.data;
    const included = this.props.fetchInitialData.included;
    if(included){
      if(included.length > 0){
        for(let i of included){
          if(i.type === "file--file"){
            bannerImages.push(i);
          }
        }
      }
    }
    let image1 = bannerImages.length > 0 ? siteUrl + bannerImages[0].attributes.url : '';
    let image2 = bannerImages.length > 0 ? siteUrl + bannerImages[1].attributes.url : '';
    let image3 = bannerImages.length > 0 ? siteUrl + bannerImages[2].attributes.url : '';

    let styles = {
      overlay: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
        backgroundColor: 'rgba(30,32,34,0.7)',
      },
      home: {
        marginTop: '72px',
        position: 'relative'
      },
      heroBanner: {
        position: 'relative',
      },
      topWidgetBlock: {
        maxWidth: 1200,
        margin: '0 auto',
      }

    }
    //console.log(bannerImages);
    const title = "lizard";
    const body = "Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica";
    return (
      <div style={styles.home}>
        <div style={styles.heroBanner}>
          <Carousel transitionTime={1500} interval={5000} dynamicHeight={true} showStatus={false} showArrows={false} showIndicators={false} showThumbs={false} autoPlay={true} infiniteLoop={true}>
            <div><img src={image1} alt="banner 1"/></div>
            <div><img src={image2} alt="banner 2"/></div>
            <div><img src={image3} alt="banner 3"/></div>
          </Carousel>
          <div className="headline"><h2>"There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</h2></div>
          <span style={styles.overlay}></span>
        </div>
        <div className="block">
          <Grid container spacing={16}>
            <SimpleCard xsmall={12} small={3} displayMedia={true} showAction={true} title={title} body={body}/>
            <SimpleCard xsmall={12} small={3} displayMedia={true} showAction={true} title={title} body={body}/>
            <SimpleCard xsmall={12} small={3} displayMedia={true} showAction={true} title={title} body={body}/>
            <SimpleCard xsmall={12} small={3} displayMedia={true} showAction={true} title={title} body={body}/>
          </Grid>
        </div>
        <div className="block block-light-grey">
          <Grid container spacing={16} justify="center" alignItems="center">
            <SimpleCard bgColor="bg-grey" xsmall={12} small={6} displayMedia={false} showAction={false} title={title} body={body}/>
            <SimpleCard bgColor="bg-grey" xsmall={12} small={6} displayMedia={false} showAction={false} title={title} body={body}/>
          </Grid>
        </div>
        <div>
        <iframe title="Cambodia Map" width="100%" height="600" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=Cambodia+(My%20Business%20Name)&amp;ie=UTF8&amp;t=&amp;z=7&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Add map to website</a></iframe>
        </div>
    </div>
    );
  }
}

export default Home;