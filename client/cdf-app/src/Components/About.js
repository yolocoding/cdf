import React, { Component } from 'react';
import  SimpleCard from './SimpleCard';

class About extends Component {
  render() {
    const about = this.props.fetchInitialData;
    return (
      <div className="About content block">
        {
          about.length && about.map((item) => {
            return <SimpleCard key={item.attributes.uuid} title={item.attributes.title} body={item.attributes.body.value}/>
          })
        }
      </div>
    );
  }
}

export default About;