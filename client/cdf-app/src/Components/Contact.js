import React, { Component } from 'react';

class Contact extends Component {
  render() {
    const contact = this.props.fetchInitialData;
    return (
      <div className="Contact">
        {
          contact.length && contact.map((item) => {
            return <li key={item.attributes.uuid}>{item.attributes.id}</li>
          })
        }
      </div>
    );
  }
}

export default Contact;