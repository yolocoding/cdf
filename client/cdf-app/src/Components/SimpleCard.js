import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import CardMedia from '@material-ui/core/CardMedia';
import ReactHtmlParser from 'react-html-parser';
import Truncate from 'react-truncate';

const styles = {
    card: {
   
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    media: {
        height: 150
    }
  };

class SimpleCard extends Component {
  
  render() {
    const displayMedia = this.props.displayMedia;
    const showAction = this.props.showAction;
    const bgColor = this.props.bgColor;
    const lines = this.props.lines ? this.props.lines : -1;
    return (
        <Grid item xs={this.props.xsmall} sm={this.props.small}>
            <Card className={bgColor} >
                { displayMedia ? <CardMedia
                style={styles.media}
                image="https://www.shoutmeloud.com/wp-content/uploads/2013/01/Free-stock-image.jpg"
                title="Contemplative Reptile"
                /> : ''}
                <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                    {this.props.title}
                </Typography>
                <Typography component="p">
                    <Truncate lines={lines} ellipsis={<span>...</span>}>
                    {ReactHtmlParser(this.props.body)}
                    </Truncate>
                </Typography>
                </CardContent>
        
                { showAction ? <CardActions>
                    <Button size="small" color="primary">
                    Share
                    </Button>
                    <Button size="small" color="primary">
                    Learn More
                    </Button>
                </CardActions> : ''}
            </Card>
        </Grid>
    );
  }
}

export default SimpleCard;