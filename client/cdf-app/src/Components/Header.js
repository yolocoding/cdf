import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Slide from '@material-ui/core/Slide';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

import { Link } from 'react-router-dom';
import logo from '../assets/logo_transparent.png';
import { styles } from './Styles';

class Header extends Component {
  constructor(){
    super();
    this.state = {
      opened: false,
    };
    this.handleChange = this.handleChange.bind(this);
  };

  handleChange = () => {
    this.setState({
      opened: !this.state.opened
    })
  }

  componentDidMount= () => {
    window.addEventListener('mousedown', this.handleBodyClick);
  }

  handleBodyClick = () => {
    this.setState({ opened: false })
  }

  render() {
    const { classes } = this.props;
    const { opened } = this.state;
    return (
      <div className={classes.root}>
      <AppBar position="fixed">
        <Toolbar className={classes.innerContainer}>
          <Typography variant="title" color="inherit" className={classes.flex}>
          <Link to="/"><img className={classes.logo} src={ logo } alt="cdf logo"/></Link>
          </Typography>
          
          <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
            <MenuIcon onClick={this.handleChange} aria-label="Collapse" />
          </IconButton>

          <Slide direction="left" in={opened} mountOnEnter unmountOnExit>
            <List className={classes.menuSection} component="nav">
              <Link to="/"><ListItem button>
                <ListItemText primary="Home" />
              </ListItem></Link>
              <Divider />
              <Link to="/about"><ListItem button>
                <ListItemText primary="About" />
              </ListItem></Link>
              <Divider />
              <Link to="/blog"><ListItem button>
                <ListItemText primary="Blog" />
              </ListItem></Link>
              <Divider />
              <Link to="/contact"><ListItem button>
                <ListItemText primary="Contact" />
              </ListItem></Link>
            </List>
          </Slide>
        </Toolbar>
      </AppBar>
    </div>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(Header);