export const styles = theme => ({

    // Layout
    innerContainer: {
      maxWidth: 1200,
      width: "100%",
      margin: '0 auto',
      justifyContent: 'center',
      alignItems:'center',
    },
    root: {
      flexGrow: 1,
    },
    flex: {
      flex: 1,
    },

    // Header.js
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      marginTop: 0,
      marginBottom: theme.spacing.unit,
      color: '#fff',
    },
    logo: {
      flex: 1,
      aspectRatio: 0,
      maxWidth: 70,
      resizeMode: 'contain',
      
    },
    menuSection: {
      width: '50%',
      maxWidth: 360,
      position: 'absolute',
      top: '0px',
      right: '0px',
      backgroundColor: theme.palette.background.paper,
      borderBottomLeftRadius: '30px',
      zIndex: 1,
      boxShadow: '0px 0px 7px rgba(0, 0, 0, 0.2)',
    },

    // Blog.js
    blogContainer: {
      padding: '25px',
      maxWidth: 1200,
      width: "100%",
    }


})