import React, { Component } from 'react';
import BlogTeaser from './BlogTeaser';
import Grid from '@material-ui/core/Grid';
import { styles } from './Styles';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

class Blog extends Component {
  render() {
    const articles = this.props.fetchInitialData;
    return (
      <div className="block blog content">
        <Grid container spacing={16}>
          {
            articles.length && articles.map((item) => {
              return <Grid key={item.attributes.uuid} item xs={12} sm={3}> <BlogTeaser blogInfo={item} /> </Grid>
              
            })
          }
        </Grid>
        
      </div>
    );
  }
}

Blog.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(Blog);