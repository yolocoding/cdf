import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import  SimpleCard from './SimpleCard';

const styles = {
    card: {
        margin: 0,
        padding: 0
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
  };

class BlogTeaser extends Component {
  render() {
    console.log(this.props.blogInfo);
    return (
      <div className="BlogTeaser">
        <SimpleCard 
        lines={5}
        showAction={true}
        title={this.props.blogInfo.attributes.title} 
        body={this.props.blogInfo.attributes.body.value} />
      </div>
    );
  }
}

BlogTeaser.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(BlogTeaser);