import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { siteUrl } from './Components/Settings';

import Home from './Components/Home';
import About from './Components/About';
import Blog from './Components/Blog';
import Contact from './Components/Contact';
import Header from './Components/Header';

// apis
const homeUrl = siteUrl + 'jsonapi/node/homepage?_format=api_json&filter[field_set_][value]=1&fields[node--homepage]=title,body,field_set_,field_homepage_banners&fields[file--file]=url&include=field_homepage_banners';
const blogUrl = siteUrl + 'jsonapi/node/article?fields[node--article]=uid,title,body,created,field_image,uuid,&fields[file--file]=url,uri&include=field_image';
const aboutUrl = siteUrl + 'jsonapi/node/about_page?_format=api_json';
const contactUrl = siteUrl + 'jsonapi/contact_form/contact_form?_format=api_json';

class App extends Component {
  constructor(){
    super();
    this.state = {
      home: [], about: [], contact: [], blog: [],
    }
  }

  componentDidMount(){
    fetch(homeUrl)
    .then((resp) => {
      return resp.json();
    }).then((res) => {
      this.setState({ home: res });
    })
    fetch(aboutUrl)
    .then((resp) => {
      return resp.json();
    }).then((res) => {
      this.setState({ about: res.data });
    })
    fetch(blogUrl)
    .then((resp) => {
      return resp.json();
    }).then((res) => {
      this.setState({ blog: res.data });
    })
    fetch(contactUrl)
    .then((resp) => {
      return resp.json();
    }).then((res) => {
      this.setState({ contact: res.data });
    })
  }

  render() {
    const articles = this.state.blog;
    const home = this.state.home;
    const about = this.state.about;
    const contact = this.state.contact;
    return (
      <Router>
        <div className="App">
          <Header />
          <Switch>
            <Route exact path="/" 
            render = {(props) => <Home {...props} fetchInitialData={home}/>} />
            <Route path="/about" 
            render = {(props) => <About {...props} fetchInitialData={about}/>} />
            <Route path="/blog" 
            render = {(props) => <Blog {...props} fetchInitialData={articles}/>} />
            <Route path="/contact" 
            render = {(props) => <Contact {...props} fetchInitialData={contact}/>} />
          </Switch>
        </div>
      </Router>
       
    );
  }
}

export default App;
