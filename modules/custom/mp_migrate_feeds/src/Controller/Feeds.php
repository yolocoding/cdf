<?php

namespace Drupal\mp_migrate_feeds\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Url;
use Drupal\mp_migrate_feeds\ExpirationHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Page listing available feeds.
 */
class Feeds extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Expiration Handler service.
   *
   * @var \Drupal\mp_migrate_feeds\ExpirationHandler
   */
  private $expirationHandler;

  /**
   * The Key Value Factory service.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  private $keyValueFactory;

  /**
   * Feeds Controller constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The Config Factory service.
   * @param \Drupal\mp_migrate_feeds\ExpirationHandler $expirationHandler
   *   The Expiration Handler service.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   *   The Key Value Factory service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, ExpirationHandler $expirationHandler, KeyValueFactoryInterface $keyValueFactory) {
    $this->configFactory = $configFactory;
    $this->expirationHandler = $expirationHandler;
    $this->keyValueFactory = $keyValueFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('mp_migrate_feeds.expiration_handler'),
      $container->get('keyvalue')
    );
  }

  /**
   * Return a table of configured feeds.
   */
  public function listFeeds() {
    $feedsConfigItems = $this->configFactory->listAll('mp_migrate_feeds.feed');
    $feedsConfigItems = $this->configFactory->loadMultiple($feedsConfigItems);

    $render = [];

    $render['feeds'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Name'),
        $this->t('Actions'),
      ],
      '#empty' => $this->t('No feeds have been created'),
    ];

    foreach ($feedsConfigItems as $feedsConfigItem) {
      $feedsConfigItem = $feedsConfigItem->get();
      $render['feeds'][] = [
        'name' => [
          '#type' => 'markup',
          '#markup' => $feedsConfigItem['label'],
        ],
        'links' => [
          '#type' => 'dropbutton',
          '#links' => [
            'edit' => [
              'title' => $this->t('Edit'),
              'url' => Url::fromRoute('mp_migrate_feeds.feed_edit', ['mp_migrate_feeds_feed_id' => $feedsConfigItem['machine_name']]),
            ],
            'expire' => [
              'title' => $this->t('Expire'),
              'url' => Url::fromRoute(
                'mp_migrate_feeds.feed_expire',
                ['mp_migrate_feeds_feed_id' => $feedsConfigItem['machine_name']],
                ['query' => ['destination' => Url::fromRoute('mp_migrate_feeds.feeds_list')->toString()]]
              ),
            ],
            'delete' => [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute(
                'mp_migrate_feeds.feed_delete',
                ['mp_migrate_feeds_feed_id' => $feedsConfigItem['machine_name']]
              ),
            ],
          ],
        ],
      ];
    }

    return $render;
  }

  /**
   * Expire content from the specified feed.
   *
   * @param string $mp_migrate_feeds_feed_id
   *   The feed id.
   */
  public function expire($mp_migrate_feeds_feed_id) {

    $config = $this->configFactory->get('mp_migrate_feeds.feed.' . $mp_migrate_feeds_feed_id);
    if (empty($config->get('lifetime'))) {
      drupal_set_message($this->t("Feed does not have a configured lifetime."));
    }
    else {
      $this->expirationHandler->expire($mp_migrate_feeds_feed_id);
      $this->keyValueFactory
        ->get('mp_migrate_feeds.last_expire')
        ->set($mp_migrate_feeds_feed_id, REQUEST_TIME);
      drupal_set_message($this->t("Feed content has been expired."));
    }

    return $this->redirect('mp_migrate_feeds.feed_edit', ['mp_migrate_feeds_feed_id' => $mp_migrate_feeds_feed_id]);
  }

}
