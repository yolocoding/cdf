<?php

namespace Drupal\mp_migrate_feeds;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigrateRowDeleteEvent;
use Drupal\migrate\Plugin\MigrateIdMapInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Service provider to expire feed content.
 */
class ExpirationHandler {

  /**
   * The Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * The node Entity Storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $nodeEntityStorage;

  /**
   * The Migrate Executable service.
   *
   * @var \Drupal\migrate\MigrateExecutableInterface
   */
  private $migrationPluginManager;

  /**
   * The Event Dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private $eventDispatcher;

  /**
   * Create a new ExpirationHandler object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The Config Factory service.
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migrationPluginManager
   *   The Migrate Executable service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The Event Dispatcher service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    MigrationPluginManagerInterface $migrationPluginManager,
    EntityTypeManagerInterface $entityTypeManager,
    EventDispatcherInterface $eventDispatcher
  ) {
    $this->configFactory = $configFactory;
    $this->migrationPluginManager = $migrationPluginManager;
    $this->nodeEntityStorage = $entityTypeManager->getStorage('node');
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * Expire content for the specified feed.
   *
   * @param string $id
   *   The feed machine name.
   */
  public function expire($id) {
    $config = $this->configFactory->get('mp_migrate_feeds.feed.' . $id);
    if (!($lifetime = $config->get('lifetime'))) {
      return;
    }
    $migration = $this->migrationPluginManager->createInstance('mp_migrate_feeds_' . $id);
    /** @var \Drupal\migrate\Plugin\MigrateDestinationInterface $destination */
    $destination = $migration->getDestinationPlugin();

    /** @var \Drupal\migrate\Plugin\MigrateIdMapInterface $idMap */
    $idMap = $migration->getIdMap();

    // @see \Drupal\migrate\MigrateExecutable::rollback()
    foreach ($idMap as $mapRow) {
      $destination_key = $idMap->currentDestination();
      if ($destination_key) {
        $map_row = $idMap->getRowByDestination($destination_key);
        if ($map_row['rollback_action'] == MigrateIdMapInterface::ROLLBACK_DELETE) {

          $entity = $this->nodeEntityStorage->load($destination_key['nid']);

          if ($entity instanceof \Drupal\node\NodeInterface) {
            $created_time = $entity->getCreatedTime();
          } else {
            continue;
          }

          if (isset($created_time)) {
            if (intval($created_time) > (strtotime('now -' . $lifetime . 'days'))) {
              continue;
            }
          }

          $this->eventDispatcher
            ->dispatch(MigrateEvents::PRE_ROW_DELETE, new MigrateRowDeleteEvent($migration, $destination_key));
          $destination->rollback($destination_key);
          $this->eventDispatcher
            ->dispatch(MigrateEvents::POST_ROW_DELETE, new MigrateRowDeleteEvent($migration, $destination_key));
        }
        // We're now done with this row, so remove it from the map.
        $idMap->deleteDestination($destination_key);
      }
      else {
        // If there is no destination key the import probably failed and we can
        // remove the row without further action.
        $source_key = $idMap->currentSource();
        $idMap->delete($source_key);
      }
    }
  }

}
