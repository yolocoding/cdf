<?php

namespace Drupal\mp_migrate_feeds\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Url;
use Drupal\mp_migrate_feeds\FeedMigrateMapper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class FeedDeleteForm.
 *
 * @package Drupal\mp_migrate_feeds\Form
 */
class FeedDeleteForm extends ConfirmFormBase {

  /**
   * The feed config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  private $config;

  private $feedMigrateMapper;

  /**
   * The Key Value Factory service.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  private $keyValueFactory;

  /**
   * Create a new FeedDeleteForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The Config Factory service.
   * @param \Drupal\mp_migrate_feeds\FeedMigrateMapper $feedMigrateMapper
   *   The Feed Migrate Mapper service.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   *   The Key Value Factory service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, FeedMigrateMapper $feedMigrateMapper, KeyValueFactoryInterface $keyValueFactory) {
    $this->configFactory = $configFactory;
    $this->feedMigrateMapper = $feedMigrateMapper;
    $this->keyValueFactory = $keyValueFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('mp_migrate_feeds.feed_migrate_mapper'),
      $container->get('keyvalue')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'mp_migrate_feeds_feed_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $mp_migrate_feeds_feed_id = '') {
    $this->config = $this->configFactory->getEditable('mp_migrate_feeds.feed.' . $mp_migrate_feeds_feed_id);

    if (empty($this->config->get('machine_name'))) {
      throw new NotFoundHttpException();
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Returns the question to ask the user.
   *
   * @return string
   *   The form question. The page title will be set to this value.
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the feed @feedName?', [
      '@feedName' => $this->config->get('label'),
    ]);
  }

  /**
   * Returns the route to go to if the user cancels the action.
   *
   * @return \Drupal\Core\Url
   *   A URL object.
   */
  public function getCancelUrl() {
    return Url::fromRoute('mp_migrate_feeds.feeds_list');
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $machineName = $this->config->get('machine_name');

    $this->feedMigrateMapper->deleteMigration($machineName);

    $this->keyValueFactory
      ->get('mp_migrate_feeds.last_expire')
      ->delete($machineName);

    $this->config->delete();

    drupal_set_message($this->t('Feed Deleted'));

    $form_state->setRedirect('mp_migrate_feeds.feeds_list');
  }

}
