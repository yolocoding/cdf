<?php

namespace Drupal\mp_migrate_feeds\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mp_migrate_feeds\FeedMigrateMapper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class FeedForm.
 *
 * @package Drupal\mp_migrate_feeds\Form
 */
class FeedEditForm extends FormBase {

  /**
   * The Entity Field Manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  private $entityFieldManager;

  /**
   * The Node Type Storage instance.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $nodeTypeStorage;

  /**
   * The Feed Migrate Mapper service.
   *
   * @var \Drupal\mp_migrate_feeds\FeedMigrateMapper
   */
  private $feedMigrateMapper;

  /**
   * Create a new FeedCreateForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity Type Manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The Entity Field Manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The Config Factory service.
   * @param \Drupal\mp_migrate_feeds\FeedMigrateMapper $feedMigrateMapper
   *   The Feed Migrate Mapper service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    EntityFieldManagerInterface $entityFieldManager,
    ConfigFactoryInterface $configFactory,
    FeedMigrateMapper $feedMigrateMapper
  ) {
    $this->entityFieldManager = $entityFieldManager;
    $this->nodeTypeStorage = $entityTypeManager->getStorage('node_type');
    $this->configFactory = $configFactory;
    $this->feedMigrateMapper = $feedMigrateMapper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('config.factory'),
      $container->get('mp_migrate_feeds.feed_migrate_mapper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mp_migrate_feeds_feed_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $mp_migrate_feeds_feed_id = '') {
    $form = [];
    $configEntity = $this->configFactory->getEditable('mp_migrate_feeds.feed.' . $mp_migrate_feeds_feed_id);

    if (empty($configEntity->get('machine_name'))) {
      throw new NotFoundHttpException();
    }

    $destinationType = $configEntity->get('destination_type');
    $nodeType = $this->nodeTypeStorage->load($destinationType);

    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Feed Settings'),
    ];
    $form['settings']['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('The name for this feed.'),
      '#default_value' => $configEntity->get('label'),
      '#required' => TRUE,
    ];

    $form['settings']['machine_name'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Machine Name'),
      '#description' => $this->t('The machine name for this feed.'),
      '#default_value' => $configEntity->get('machine_name'),
      '#disabled' => TRUE,
      '#machine_name' => [
        'source' => ['settings', 'label'],
        'exists' => function ($value, $element, $form_state) {
          // Since the field is disabled, this shouldn't be executed.
          return FALSE;
        },
      ],
    ];
    $form['settings']['destination_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Destination Content Type'),
      '#description' => $this->t('Destination Content Type cannot be changed after feed creation'),
      '#options' => [
        $destinationType => $nodeType->get('name'),
      ],
      '#default_value' => $destinationType,
      '#disabled' => TRUE,
    ];

    $form['settings']['source_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source URL'),
      '#description' => $this->t('Source URL for the feed'),
      '#default_value' => $configEntity->get('source_url'),
      '#required' => TRUE,
    ];

    $form['settings']['source_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Source Format'),
      '#description' => $this->t('The data format of the source feed'),
      '#options' => [
        'xml' => 'XML',
        'json' => 'JSON',
      ],
      '#default_value' => $configEntity->get('source_type'),
      '#required' => TRUE,
    ];

    $form['settings']['item_selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Item Selector'),
      '#description' => $this->t('The selector to find the list of items from the feed.'),
      '#default_value' => $configEntity->get('item_selector'),
      '#required' => TRUE,
    ];

    $form['settings']['lifetime'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Lifetime'),
      '#description' => $this->t('The duration in days after which feed items will be removed from the site. Leave empty for items to not expire.'),
      '#default_value' => $configEntity->get('lifetime'),
    ];

    $form['fields_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Fields'),
    ];
    $form['fields_wrapper']['fields'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('id'),
        $this->t('Label'),
        $this->t('Selector'),
        $this->t('Data Type'),
        $this->t('Destination Field'),
      ],
    ];

    $fields = $configEntity->get('fields');
    $fields[] = [
      'label' => '',
      'machine_name' => '',
      'selector' => '',
      'data_type' => '',
      'destination_field' => '',
    ];

    // Get the available node fields to map to, excluding non-content fields.
    $nodeFieldOptions = [
      '' => '',
    ];
    $nodeFieldDefinitions = $this->entityFieldManager->getFieldDefinitions('node', $nodeType->id());
    foreach ($nodeFieldDefinitions as $fieldDefinition) {
      $nodeFieldOptions[$fieldDefinition->getName()] = $fieldDefinition->getLabel();
    }
    $nodeFieldOptions = array_diff_key(
      $nodeFieldOptions,
      array_flip([
        'nid',
        'uuid',
        'vid',
        'langcode',
        'type',
        'revision_timestamp',
        'revision_uid',
        'revision_log',
        'status',
        'uid',
        'promote',
        'sticky',
        'default_langcode',
        'revision_translation_affected',
        'moderation_state',
        'metatag',
        'path',
        'menu_link',
      ])
    );
    // Remove the "Authored By" option from the destination field
    // in order to prevent users from mapping feed data to it.
    unset($nodeFieldOptions['created']);

    $fieldIndex = -1;
    foreach ($fields as $field) {
      $fieldIndex++;

      $form['fields_wrapper']['fields'][$fieldIndex] = [
        'id' => [
          '#type' => 'checkbox',
          '#default_value' => $configEntity->get('ids') && in_array($field['machine_name'], $configEntity->get('ids')),
        ],
        'name_wrapper' => [
          'label' => [
            '#type' => 'textfield',
            '#parents' => ['fields', $fieldIndex, 'label'],
            '#default_value' => $field['label'],
            '#size' => 16,
          ],
          'machine_name' => [
            '#type' => 'machine_name',
            '#parents' => ['fields', $fieldIndex, 'machine_name'],
            '#title' => $this->t('Machine Name'),
            '#default_value' => $field['machine_name'],
            '#size' => 16,
            '#required' => FALSE,
            '#machine_name' => [
              'source' => ['fields_wrapper', 'fields', $fieldIndex, 'name_wrapper', 'label'],
              'exists' => function ($value, $element, $form_state) {
                // TODO ensure that machine name is unique within this feed.
                return FALSE;
              },
            ],
            '#states' => [
              'required' => [
                '[data-drupal-selector="edit-fields-' . $fieldIndex . '-name"]' => ['!value' => ''],
              ],
            ],
          ],
        ],
        'selector' => [
          '#type' => 'textfield',
          '#default_value' => $field['selector'],
          '#size' => 30,
        ],
        'data_type_wrapper' => [
          'data_type' => [
            '#type' => 'select',
            '#parents' => ['fields', $fieldIndex, 'data_type'],
            '#options' => [
              'string' => $this->t('String'),
//              'integer' => $this->t('Integer'),
              'date' => $this->t('Date'),
              'value' => $this->t('Fixed Value'),
              'reference' => $this->t('Reference'),
            ],
            '#default_value' => $field['data_type'],
          ],
          'data_type_date' => [
            '#type' => 'container',
            '#states' => [
              'visible' => [
                '[data-drupal-selector="edit-fields-' . $fieldIndex . '-data-type"]' => ['value' => 'date'],
              ],
            ],
            'from_format' => [
              '#type' => 'textfield',
              '#parents' => ['fields', $fieldIndex, 'from_format'],
              '#title' => 'From Format',
              '#default_value' => isset($field['from_format']) ? $field['from_format'] : '',
              '#size' => 16,
            ],
            'to_format' => [
              '#type' => 'textfield',
              '#parents' => ['fields', $fieldIndex, 'to_format'],
              '#title' => 'To Format',
              '#default_value' => isset($field['to_format']) ? $field['to_format'] : '',
              '#size' => 16,
            ],
          ],
        ],
        'destination_field' => [
          '#type' => 'select',
          '#options' => $nodeFieldOptions,
          '#default_value' => $field['destination_field'],
        ],
      ];
    }

    $form['fields_wrapper']['fields_help'] = [
      '#type' => 'container',
      '#markup' => $this->t('<em>To remove a field, clear its name input before saving.</em>'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (!UrlHelper::isValid($form_state->getValue('source_url'))) {
      $form_state->setErrorByName('source_url', $this->t('Source URL is invalid'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('mp_migrate_feeds.feed.' . $form_state->getValue('machine_name'));

    $config
      ->set('label', $form_state->getValue('label'))
      ->set('source_url', $form_state->getValue('source_url'))
      ->set('source_type', $form_state->getValue('source_type'))
      ->set('item_selector', $form_state->getValue('item_selector'));

    $lifetime = $form_state->getValue('lifetime');
    if (empty($lifetime)) {
      $config->clear('lifetime');
    }
    else {
      $config->set('lifetime', $lifetime);
    }

    $ids = [];
    $fields = [];

    foreach ($form_state->getValue('fields') as $fieldIndex => $fieldFormData) {
      if (empty($fieldFormData['label'])) {
        continue;
      }

      $field = [
        'label' => $fieldFormData['label'],
        'machine_name' => $fieldFormData['machine_name'],
        'selector' => $fieldFormData['selector'],
        'data_type' => $fieldFormData['data_type'],
        'destination_field' => $fieldFormData['destination_field'],
      ];

      if ($fieldFormData['data_type'] == 'date') {
        $field['from_format'] = $fieldFormData['from_format'];
        $field['to_format'] = $fieldFormData['to_format'];
      }

      $fields[] = $field;

      if (!empty($fieldFormData['id'])) {
        $ids[] = $fieldFormData['machine_name'];
      }
    }
    $config->set('ids', $ids);
    $config->set('fields', $fields);

    $config->save();

    $this->feedMigrateMapper->updateMigration($config->get('machine_name'));

    drupal_set_message($this->t(
      'Feed %feedName has been updated.',
      ['%feedName' => $form_state->getValue('label')]
    ));
  }

}
