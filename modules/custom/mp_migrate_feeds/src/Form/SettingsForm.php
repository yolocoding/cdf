<?php

namespace Drupal\mp_migrate_feeds\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\mp_migrate_feeds\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'mp_migrate_feeds.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mp_migrate_feeds_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mp_migrate_feeds.settings');

    $form['expire'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Expiration Settings'),
      '#description' => $this->t('Processing a feed with a large number of items can take a long time.  Adjust settings here to avoid impacting other cron tasks.'),
    ];

    $form['expire']['limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Limit'),
      '#description' => $this->t('Maximum number of feeds to process on each cron run'),
      '#min' => 1,
      '#step' => 1,
      '#default_value' => $config->get('expire.limit'),
    ];

    $form['expire']['period'] = [
      '#type' => 'number',
      '#title' => $this->t('Period'),
      '#description' => $this->t('Delay between processing a single feed in hours.'),
      '#min' => 0,
      '#step' => 1,
      '#default_value' => $config->get('expire.period'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('mp_migrate_feeds.settings')
      ->set('expire', [
        'limit' => $form_state->getValue('limit'),
        'period' => $form_state->getValue('period'),
      ])
      ->save();
  }

}
