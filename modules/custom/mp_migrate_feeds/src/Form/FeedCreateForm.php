<?php

namespace Drupal\mp_migrate_feeds\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mp_migrate_feeds\FeedMigrateMapper;
use Drupal\node\Entity\NodeType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FeedForm.
 *
 * @package Drupal\mp_migrate_feeds\Form
 */
class FeedCreateForm extends FormBase {

  /**
   * The Node Type Storage instance.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $nodeTypeStorage;

  /**
   * The Feed Migrate Mapper service.
   *
   * @var \Drupal\mp_migrate_feeds\FeedMigrateMapper
   */
  private $feedMigrateMapper;

  /**
   * Create a new FeedCreateForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity Type Manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The Config Factory service.
   * @param \Drupal\mp_migrate_feeds\FeedMigrateMapper $feedMigrateMapper
   *   The Feed Migrate Mapper service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    ConfigFactoryInterface $configFactory,
    FeedMigrateMapper $feedMigrateMapper
  ) {
    $this->nodeTypeStorage = $entityTypeManager->getStorage('node_type');
    $this->configFactory = $configFactory;
    $this->feedMigrateMapper = $feedMigrateMapper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('mp_migrate_feeds.feed_migrate_mapper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mp_migrate_feeds_feed_create_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('The name for this feed.'),
      '#default_value' => '',
      '#required' => TRUE,
    ];

    $form['machine_name'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Machine Name'),
      '#description' => $this->t('The machine name for this feed.'),
      '#default_value' => '',
      '#required' => TRUE,
      '#machine_name' => [
        'source' => ['label'],
        'exists' => function ($value, $element, $form_state) {
          $existingConfig = $this->configFactory->get('mp_migrate_feeds.feed.' . $value);
          return !empty($existingConfig->get('machine_name'));
        },
      ],
    ];

    $nodeTypes = ['' => ''] + array_map(
        function (NodeType $element) {
          return $element->get('name');
        },
        $this->nodeTypeStorage->loadMultiple()
      );
    $form['destination_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Destination Content Type'),
      '#options' => $nodeTypes,
      '#required' => TRUE,
    ];

    $form['source_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source URL'),
      '#description' => $this->t('Source URL for the feed'),
      '#required' => TRUE,
    ];

    $form['source_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Source Format'),
      '#description' => $this->t('The data format of the source feed'),
      '#options' => [
        'xml' => 'XML',
        'json' => 'JSON',
      ],
      '#default_value' => 'xml',
      '#required' => TRUE,
    ];

    $form['item_selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Item Selector'),
      '#description' => $this->t('The selector to find the list of items from the feed.'),
      '#required' => TRUE,
    ];

    $form['lifetime'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Lifetime'),
      '#description' => $this->t('The duration in days after which feed items will be removed from the site. Leave empty for items to not expire.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if (!UrlHelper::isValid($form_state->getValue('source_url'))) {
      $form_state->setErrorByName('source_url', $this->t('Source URL is invalid'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('mp_migrate_feeds.feed.' . $form_state->getValue('machine_name'));

    $config
      ->set('machine_name', $form_state->getValue('machine_name'))
      ->set('label', $form_state->getValue('label'))
      ->set('destination_type', $form_state->getValue('destination_type'))
      ->set('source_url', $form_state->getValue('source_url'))
      ->set('source_type', $form_state->getValue('source_type'))
      ->set('item_selector', $form_state->getValue('item_selector'));

    if (($lifetime = $form_state->getValue('lifetime'))) {
      $config->set('lifetime', $lifetime);
    }

    $config->save();

    $this->feedMigrateMapper->updateMigration($form_state->getValue('machine_name'));

    drupal_set_message($this->t(
      'Feed %feedName has been created.',
      ['%feedName' => $form_state->getValue('name')]
    ));

    $form_state->setRedirect(
      'mp_migrate_feeds.feed_edit',
      ['mp_migrate_feeds_feed_id' => $form_state->getValue('machine_name')]
    );
  }

}
