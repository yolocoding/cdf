<?php

namespace Drupal\mp_migrate_feeds;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\MigrateMessage;

/**
 * Map feed configuration to Migrate Plus configuration entities.
 */
class FeedMigrateMapper {

  /**
   * Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * The Entity Field Manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  private $entityFieldManager;

  /**
   * The Cache Tags Invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  private $cacheTagsInvalidator;

  /**
   * FeedMigrateMapper constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The Config Factory service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The Entity Field Manager service.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cacheTagsInvalidator
   *   The Cache Tags Invalidator service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    EntityFieldManagerInterface $entityFieldManager,
    CacheTagsInvalidatorInterface $cacheTagsInvalidator
  ) {
    $this->configFactory = $configFactory;
    $this->entityFieldManager = $entityFieldManager;
    $this->cacheTagsInvalidator = $cacheTagsInvalidator;
  }

  /**
   * Update the migration entity for the specified feed.
   *
   * @param string $id
   *   The feed machine name.
   */
  public function updateMigration($id) {
    $feedConfig = $this->configFactory->get('mp_migrate_feeds.feed.' . $id);
    $migrateConfig = $this->configFactory->getEditable('migrate_plus.migration.mp_migrate_feeds_' . $id);

    $destinationNodeTypeFields = $this->entityFieldManager->getFieldDefinitions('node', $feedConfig->get('destination_type'));

    // Common static values.
    $migrateConfig
      ->set('id', 'mp_migrate_feeds_' . $id)
      ->set('label', $feedConfig->get('label'))
      ->set('migration_group', 'feeds')
      ->set('source', [
        'plugin' => 'url',
        'data_fetcher_plugin' => 'http',
        'headers' => [],
      ])
      ->set('destination', [
        'plugin' => 'entity:node',
      ])
      ->set('dependencies', [
        'enforced' => [
          'module' => ['mp_migrate_feeds'],
        ],
      ]);

    // Feed-specific values.
    switch ($feedConfig->get('source_type')) {
      case 'xml':
        $migrateConfig->set('source.data_parser_plugin', 'simple_xml');
        break;

      case 'json':
        $migrateConfig->set('source.data_parser_plugin', 'json');
        break;
    }
    $migrateConfig->set('source.urls', $feedConfig->get('source_url'));
    $migrateConfig->set('source.item_selector', $feedConfig->get('item_selector'));

    $idFields = $feedConfig->get('ids');
    $migrateSourceFields = [];
    $migrateIdFields = [];
    $migrateProcess = [
      'type' => [
        'plugin' => 'default_value',
        'default_value' => $feedConfig->get('destination_type'),
      ],
    ];

    $fields = $feedConfig->get('fields') ?: [];
    foreach ($fields as $field) {
      if ($field['data_type'] != 'value') {
        $migrateSourceFields[] = [
          'name' => $field['machine_name'],
          'label' => $field['label'],
          'selector' => $field['selector'],
        ];
      }

      if (!empty($field['destination_field'])) {
        if ($field['data_type'] == 'date') {
          $migrateProcess[$field['destination_field']] = [
            'plugin' => 'format_date',
            'from_format' => $field['from_format'],
            'to_format' => $field['to_format'],
            'source' => $field['machine_name'],
            'settings' => [
              'validate_format' => FALSE,
            ],
          ];
        }
        elseif ($field['data_type'] == 'value') {
          $migrateProcess[$field['destination_field']] = [
            'plugin' => 'default_value',
            'default_value' => $field['selector'],
          ];
        }
        elseif ($field['data_type'] == 'reference') {
          $migrateProcess[$field['destination_field']] = [
            'plugin' => 'entity_generate',
            'source' => $field['machine_name'],
          ];
        }
        else {
          // TODO handle integers different from strings?
          $migrateProcess[$field['destination_field']] = $field['machine_name'];
        }

        if ($destinationNodeTypeFields[$field['destination_field']]->getType() == 'entity_reference' && $field['data_type'] != 'reference') {
          $migrateProcess[$field['destination_field']] = [
            $migrateProcess[$field['destination_field']],
            [
              'plugin' => 'entity_generate',
            ],
          ];
        }
      }

      if (in_array($field['machine_name'], $idFields)) {
        $migrateIdFields[$field['machine_name']] = [
          // TODO handle integers different from strings?
          'type' => 'string',
        ];
      }
    }

    if ($migrateConfig->getOriginal('source.ids') != $migrateIdFields) {
      $this->clearIdMap($id);
    }

    $migrateConfig
      ->set('source.fields', $migrateSourceFields)
      ->set('source.ids', $migrateIdFields)
      ->set('process', $migrateProcess);

    $migrateConfig->save();

    $this->cacheTagsInvalidator->invalidateTags(['migration_plugins']);
  }

  /**
   * Delete the migration entity for the specified feed.
   *
   * @param string $id
   *   The feed machine name.
   */
  public function deleteMigration($id) {
    $this->clearIdMap($id);

    $this->configFactory
      ->getEditable('migrate_plus.migration.mp_migrate_feeds_' . $id)
      ->delete();

    $this->cacheTagsInvalidator->invalidateTags(['migration_plugins']);
  }

  /**
   * Rollback the migration and destroy the existing ID Map.
   *
   * @param string $id
   *   The feed id.
   */
  private function clearIdMap($id) {
    /** @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migrationManager */
    $migrationManager = \Drupal::service('plugin.manager.migration');
    /** @var \Drupal\migrate\Plugin\MigrationInterface $migration */
    $migration = $migrationManager->createInstance('mp_migrate_feeds_' . $id);
    $migrateMessage = new MigrateMessage();
    $executable = new MigrateExecutable($migration, $migrateMessage);
    $executable->rollback();
    $migration->getIdMap()->destroy();
  }

}
